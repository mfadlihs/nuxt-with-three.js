export default async (path, queries) => {
  const headers = authHeaders()

  const { data, error } = await useSuitapiData(path, {
    headers,
    ...queries
  })

  if (error.value) {
    // showError({
    //   ...error.value,
    //   statusMessage: 'Unable to fetch' + path
    // })
    // console.log(error.value)

    // Bisa dipertimbangkan error handlingnya yang gak bikin seluruh halaman jadi error page, jadi errornya lacally tiap komponen
    return error
  }

  return data
}
