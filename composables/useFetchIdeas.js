export default async (filters) => {
    // const { data, error, pending } = await useSuitapiData(`/api/contents`, {
    //         ...filters
    //     }
    // ) 
    // API Party proxy
    
    const { data, error, pending } = await useFetch('/api/strapi/contents', {
            ...filters
        }
    )
    // Proxy from folder /server/[proxyname]
    
    
    if (error.value) {
        throw createError({
            ...error.value,
            statusMessage: "Unable to fetch ideas"
        })
    }

    return {data, error, pending}
}