import { defineNuxtConfig } from 'nuxt/config'
import defaultMeta from './config/defaultMeta'
const version = process.env.npm_package_version
const isDev = () =>
  process.env.NODE_ENV === 'test' || process.env.NODE_ENV === 'development'

let noIndexing: { hid: string; name: string; content: string }[] = []
const isStaging = Number(process.env.IS_STAGING)

if (isStaging === 1) {
  noIndexing = [
    {
      hid: 'robots',
      name: 'robots',
      content: 'noindex'
    },
    {
      hid: 'googlebot',
      name: 'googlebot',
      content: 'noindex'
    }
  ]
}

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  app: {
    buildAssetsDir: !isDev() ? 'assets' : '/_nuxt/',
    head: {
      title: 'Baze Nuxt 3',
      charset: 'utf-8',
      viewport: 'width=device-width, initial-scale=1',
      meta: [...defaultMeta, ...noIndexing],
      link: [
        { rel: 'apple-touch-icon', href: '/apple-icon.png' },
        { rel: 'icon', type: 'image/png', href: '/favicon.png' }
      ]
    }
  },

  devtools: {
    enabled: true,

    timeline: {
      enabled: true
    }
  },

  imports: {
    dirs: ['composables', 'composables/**', 'utils']
  },

  runtimeConfig: {
    API_URL: process.env.API_URL,
    NUXT_SECRET: process.env.NUXT_SECRET,
    RECAPTCHA_SITE_KEY: process.env.RECAPTCHA_SITE_KEY,
    public: {
      BASE_URL: process.env.NUXT_PUBLIC_SITE_URL
    }
  },

  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {}
    }
  },

  css: [
    '~/assets/scss/main.scss',
    '~/assets/scss/modules/_modules.scss',
    '~/assets/scss/modules/_header.scss',
    '@vuepic/vue-datepicker/dist/main.css',
    'vue-select/dist/vue-select.css'
  ],

  plugins: [
    { src: '~/plugins/vue-select.js' },
    { src: '~/plugins/recaptcha.js' }
  ],

  modules: [
    '@sidebase/nuxt-auth',
    '@nuxtjs/tailwindcss',
    '@nuxt/image',
    '@vueuse/nuxt',
    '@pinia/nuxt',
    'nuxt-jsonld',
    // 'nuxt-swiper',
    'nuxt-icons',
    '@nuxtjs/i18n',
    'dayjs-nuxt',
    '@nuxtjs/sitemap',
    'nuxt-api-party',
    '@nuxtjs/google-fonts',
    '@sidebase/nuxt-auth',
    'nuxt-snackbar',
    '@nuxtjs/google-fonts',
    '@nuxtjs/eslint-module',
    './modules/auto-import-eslint.ts'
  ],

  pinia: {
    autoImports: [
      // automatically imports `defineStore`
      'defineStore'
    ]
  },

  auth: {
    // provider: {
    //   type: 'local',
    //   endpoints: {
    //     signIn: { path: '/local' },
    //     getSession: { path: '/user' }
    //   },
    //   pages: {
    //     login: '/login'
    //   },
    //   token: {
    //     signInResponseTokenPointer: '/token/accessToken'
    //   },
    // },
    // globalAppMiddleware: {
    //   isEnabled: true
    // }
    baseURL: process.env.NUXT_PUBLIC_SITE_URL,
    provider: {
      type: 'authjs'
    },
    globalAppMiddleware: {
      isEnabled: true
    }
  },

  i18n: {
    strategy: 'prefix',
    defaultLocale: 'en',
    locales: [
      {
        code: 'id',
        iso: 'id-ID'
      },
      {
        code: 'en',
        iso: 'en-US'
      }
    ],
    vueI18n: './lang/localeConfig.js',
    detectBrowserLanguage: false
  },

  dayjs: {
    locales: ['en', 'id'],
    defaultLocale: 'en',
  },

  googleFonts: {
    families: {
      Roboto: true,
      Montserrat: true
    }
  },

  build: {
    transpile: ['@vuepic/vue-datepicker', 'jsonwebtoken']
  },

  image: {
    domains:
      isStaging === 1
        ? ['assets.suitdev.com', 'suitdev.com', 'img.youtube.com']
        : ['suitmedia.static-assets.id'],
    alias: {
      suitmedia:
        isStaging === 1
          ? 'https://assets.suitdev.com'
          : 'https://suitmedia.static-assets.id',
      youtube: 'https://img.youtube.com'
    },
    presets: {
      thumbnail: {
        modifiers: {
          format: 'webp',
          width: 450,
          height: 254
        }
      },
      client: {
        modifiers: {
          format: 'webp',
          width: 204,
          height: 204
        }
      },
      heroDetail: {
        modifiers: {
          format: 'webp',
          width: 960,
          height: 540
        }
      }
    },
    provider: 'ipx',
    ipx: {
      maxAge: 60000
    }
  },

  // This is the amount of milliseconds to cache the sitemap for.
  sitemap: {
    defaults: {
      changefreq: 'daily',
      priority: 0.8,
      lastmod: new Date()
    },
    cacheTtl: 1000 * 60 * 60 * 24 // 1 day
  },

  apiParty: {
    endpoints: {
      suitapi: {
        url: process.env.API_URL!,
        // token: `Bearer ${process.env.API_TOKEN!}`,
        headers: {
          accept: 'application/json'
          // Authorization: `Bearer ${process.env.API_TOKEN!}`
        }
      }
    }
  },

  snackbar: {
    top: true,
    right: true,
    duration: 5000
  },

  vite: {
    plugins: [
      // sentryVitePlugin({
      //   url: process.env.SENTRY_DSN,
      //   org: 'suitmedia',
      //   project: 'suitmedia-frontend'
      // })
    ],
    optimizeDeps: { exclude: ['fsevents'] },

    build: {
      rollupOptions: {
        output: {
          entryFileNames: `assets/[hash]-${version}.js`,
          chunkFileNames: `assets/[hash]-${version}.js`,
          assetFileNames: assetInfo => {
            if (assetInfo?.name?.endsWith('.css'))
              return `assets/css/[hash]-${version}[extname]`
            if (/\.(png|jpe?g|gif|svg|webp)$/i.test(assetInfo?.name || ''))
              return `assets/img/[hash]-${version}[extname]`
            if (/\.(woff|woff2|eot|ttf|otf)$/i.test(assetInfo?.name || ''))
              return `assets/fonts/[hash]-${version}[extname]`
            if (/\.(mp4|webm|ogv)$/i.test(assetInfo?.name || ''))
              return `assets/videos/[hash]-${version}[extname]`
            return `assets/[hash]-${version}[extname]`
          }
        }
      }
    },

    css: {
      preprocessorOptions: {
        scss: {
          additionalData: '@use "~/assets/scss/partials/_variables.scss" as *;'
        }
      }
    }
  }
})
