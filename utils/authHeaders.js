export default () => {
  const { data } = useAuth()
  // Value token bisa berbeda tergantung dari API developernya, contoh:
  // data.value?.user.data.jwt
  // data.value?.user.data.user.token
  // data.value?.user.data.user.access_token dan lain sebagainya
  // cara mengetahuinya bisa dengan console.log atau print ke <pre><code>
  console.log({data: data.value})
  const token = data.value?.user.data.token
  return {
    accept: 'application/json',
    Authorization: `Bearer ${token}`
  }
}