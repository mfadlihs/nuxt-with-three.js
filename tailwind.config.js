const lighten = (clr, val) => `hsl(from var(${clr}) h s calc(l + ${val}))`;
const darken = (clr, val) => `hsl(from var(${clr}) h s calc(l - ${val}))`;
const primaryColor = '--primary-color';
const secondaryColor = '--secondary-color';

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './components/**/*.{js,vue,ts}',
    './layouts/**/*.vue',
    './pages/**/*.vue',
    './plugins/**/*.{js,ts}',
    './nuxt.config.{js,ts}',
    './app.vue'
  ],
  theme: {
    extend: {
      colors: {
        transparent: 'transparent',
        primary: {
          DEFAULT: lighten(primaryColor, 0),
          50: lighten(primaryColor, 45),
          100: lighten(primaryColor, 40),
          150: lighten(primaryColor, 35),
          200: lighten(primaryColor, 30),
          300: lighten(primaryColor, 20),
          400: lighten(primaryColor, 10),
          500: lighten(primaryColor, 0),
          600: darken(primaryColor, 10),
          700: darken(primaryColor, 20),
        },
        secondary: {
          DEFAULT: lighten(secondaryColor, 0),
          50: lighten(secondaryColor, 45),
          100: lighten(secondaryColor, 40),
          150: lighten(secondaryColor, 35),
          200: lighten(secondaryColor, 30),
          300: lighten(secondaryColor, 20),
          400: lighten(secondaryColor, 10),
          500: lighten(secondaryColor, 0),
          600: darken(secondaryColor, 10),
          700: darken(secondaryColor, 20),
          800: darken(secondaryColor, 30),
          900: darken(secondaryColor, 40),
        },
        red: '#ef4444'
      }
    }

    // fontFamily: {
    //   sans: ['Montserrat'],
    //   serif: ['Montserrat'],
    //   mono: ['Montserrat'],
    //   display: ['Montserrat'],
    //   body: ['Montserrat']
    // }
  },
  plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')]
}
