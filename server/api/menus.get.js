// import menus from '@/data/menus.json'
import { getQuery } from 'h3'

export default defineEventHandler((event) => {
  const query = getQuery(event)

  const menus = {
    en: [
      {
        title: "About Us",
        slug: '/about-us',
        children: []
      },
      {
        title: "Components",
        slug: '/components',
        children: [
          {
            title: "Colors",
            slug: '/colors',
            children: []
          },
          {
            title: "Buttons",
            slug: '/buttons',
            children: []
          },
          {
            title: "Forms",
            slug: '/forms',
            children: []
          },
          {
            title: "Flash notif",
            slug: '/flash-notif',
            children: []
          },
          {
            title: "Cards",
            slug: '/cards',
            children: []
          },
          {
            title: "Tabs",
            slug: '/tabs',
            children: []
          },
        ]
      },
      {
        title: "Blocks",
        slug: '/blocks',
        children: [
          {
            title: "Hero",
            slug: '/heroes',
            children: []
          },
          {
            title: "List Cards",
            slug: '/list-cards',
            children: []
          },
        ]
      },
    ],
    id: [
      {
        title: "Tentang Kami",
        slug: '/tentang-kami',
        children: []
      },
      {
        title: "Komponen",
        slug: '/komponen',
        children: [
          {
            title: "Warna",
            slug: '/warna',
            children: []
          },
          {
            title: "Button",
            slug: '/button',
            children: []
          },
          {
            title: "Form",
            slug: '/form',
            children: []
          },
          {
            title: "Flash notif",
            slug: '/flash-notif',
            children: []
          },
          {
            title: "Card",
            slug: '/card',
            children: []
          },
          {
            title: "Tab",
            slug: '/tab',
            children: []
          },
        ]
      },
      {
        title: "Block",
        slug: '/block',
        children: [
          {
            title: "Hero",
            slug: '/hero',
            children: []
          },
          {
            title: "List Card",
            slug: '/list-card',
            children: []
          },
        ]
      },
    ],
  }

  let menuLocale = menus.en

  if (query.locale !== undefined) {
    menuLocale = menus[query.locale]
  }

  return menuLocale
})
