
export default cachedEventHandler(
  async (e) => {
    const api = process.env.API_URL
    const [
      ideas
    ] = await Promise.all([
      $fetch(`${api}/api/ideas`, {
        headers: {
          accept: 'application/json'
        },
        params: {
          'fields[ideas]': 'slug,updated_at',
          sort: '-published_at',
          'page[size]': 100
        }
      }),
    ])
    return [...ideas.data].map((p) => {
      return { loc: `/ideas/${p.slug}`, lastmod: p.updated_at, changefreq: 'daily',
      priority: 0.8, }
    })
  },
  {
    name: 'sitemap-dynamic-url',
    maxAge: 60 * 10, // cache URLs for 10 minutes
  }
);