// file: ~/server/api/auth/[...].ts
import GoogleProvider from 'next-auth/providers/google'
import CredentialsProvider from 'next-auth/providers/credentials'
import { NuxtAuthHandler } from '#auth'

const rtConfig = useRuntimeConfig()
const apiUrl = rtConfig.API_URL

export default NuxtAuthHandler({
  secret: rtConfig.NUXT_SECRET,

  pages: {
    signIn: '/en/login',
    error: '/en/login'
  },

  providers: [
    // @ts-expect-error You need to use .default here for it to work during SSR. May be fixed via Vite at some point
    GoogleProvider.default({
      clientId:
        '45647372927-8liogmcpm7b9jov44acrkfbv5fa3n973.apps.googleusercontent.com',
      clientSecret: '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',
      authorization: {
        params: {
          prompt: 'consent',
          access_type: 'offline',
          response_type: 'code'
        }
      },
      httpOptions: {
        timeout: 40000
      }
    }),
    // @ts-expect-error
    CredentialsProvider.default({
      name: 'credentials',
      credentials: {},
      async authorize(credentials: { email: string; password: string }) {
        const res = await $fetch(`${apiUrl}/api/auth/login`, {
          method: 'POST',
          body: JSON.stringify({
            email: credentials.email,
            password: credentials.password
          })
        })

        // console.log({res})

        if (res.code !== 200) {
          throw createError({
            statusCode: 401,
            statusMessage: 'Unauthorized'
          })
        }

        return {
          ...res,
          password: undefined
        }
      }
    })
  ],

  session: {
    strategy: 'jwt'
  },

  callbacks: {
    // eslint-disable-next-line require-await
    async jwt({ token, user, account }) {
      if (user) {
        token = {
          ...token,
          ...user
        }
      }

      return token
    },

    // eslint-disable-next-line require-await
    async session({ session, token }) {
      session.user = {
        ...token,
        ...session.user
      }

      return session
    }
  }
})
