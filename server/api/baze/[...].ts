import { joinURL } from 'ufo'
import { proxyRequest } from 'h3'

// eslint-disable-next-line require-await
export default defineEventHandler(async (event) => {
  const proxyUrl = useRuntimeConfig().API_URL
  const path = event.path.replace(/^\/api\/baze\//, '/api/')
  const target = joinURL(proxyUrl, path)
  console.log({proxyUrl, path, target})
  
  return proxyRequest(event, target)
  // return target
})
