import { joinURL } from 'ufo'
import { proxyRequest } from 'h3'

// eslint-disable-next-line require-await
export default defineEventHandler(async (event: { path: string }) => {
  const proxyUrl = useRuntimeConfig().API_URL
  const path = event.path.replace(/^\/api\/strapi\//, '/api/')

  
  const target = joinURL(proxyUrl, path)

  return proxyRequest(event, target)
  // return target
})
